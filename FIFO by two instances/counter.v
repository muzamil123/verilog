module counter
	#( 
		parameter 		DEPTH = 32,
		parameter 		COUNT_WIDTH = $clog2(DEPTH)
	)
	(
		input 			clk,
		input 			reset,
		input 			increment,
		output reg 		[COUNT_WIDTH:0] count
	);


	always @(posedge clk)
	begin 
		if(reset)
			count <=  0;
		else if(increment)
			count <=  count+1;
	end 

endmodule