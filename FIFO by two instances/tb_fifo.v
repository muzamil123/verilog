module tb_fifo; 

	parameter 	DATA_WIDTH = 16;
	reg 		clk;
	reg 		reset;
	reg 		[DATA_WIDTH-1:0] dataIn;
	wire 		[DATA_WIDTH-1:0] dataOutput;
	reg 		push;
	reg 		pop;
	wire 		fifoEmpty;
	wire 		fifoFull;


	circular_fifo cr_instance 
	 (
		.clk			(clk),
		.reset			(reset),
		.dataOutput		(dataOutput),
		.dataIn			(dataIn),
		.push			(push),
		.fifoFull		(fifoFull),
		.fifoEmpty		(fifoEmpty),
		.pop			(pop)
	);

	initial begin
		clk = 0;
		forever #5 clk = ~clk;
	end

	initial begin
		$dumpfile("two_counter_fifo_wave.vcd");
		$dumpvars(2,cr_instance);
		
		reset=0;
		push=0;
		pop=0;
		@(posedge clk)
		 reset = #1 1;
		@(posedge clk)
		 reset = #1 0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
			@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=111;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1  push =0;
		
		repeat (5) @(posedge clk);
		
		@(posedge clk);
		#1 pop=1;
		push=0;
		@(posedge clk);
		#1 pop=1;
		push=0;
		@(posedge clk);
		#1 pop=1;
		push=0;
		@(posedge clk);
		#1 pop=1;
		push=0;	
		@(posedge clk);
		#1 pop=0;
		push=0;
		
		repeat (5) @(posedge clk);
		
		@(posedge clk);
		#1 push =1;
		dataIn=18;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=19;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=11;
		pop=0;
		@(posedge clk);
		#1 push =1;
		dataIn=21;
		pop=0;
		@(posedge clk);
		#1 push =0;

		@(posedge clk);
		#1 pop=1;
		push=0;
		@(posedge clk);
		#1 pop=0;
		@(posedge clk);
		#1 push=1;
		dataIn=10;
		pop=0;
		@(posedge clk);
		#1 push=0;
		
		repeat (20) @(posedge clk);

		$finish;
	end

endmodule 