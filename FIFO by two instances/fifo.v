module circular_fifo 
	#(
		parameter 	DATA_WIDTH = 16,
		parameter 	DEPTH = 32,
		parameter 	COUNT_WIDTH = $clog2(DEPTH)
	)

	(
		input 						clk,
		input 						reset,
		input 						push,
		input 						pop,
		input 	[DATA_WIDTH-1:0] 	dataIn,
		input 	[COUNT_WIDTH:0]		wr_ptr,
		input 	[COUNT_WIDTH:0]  	rd_ptr,
		output 	[DATA_WIDTH-1:0] 	dataOutput,
		output  					fifoFull,
		output  					fifoEmpty
	);

		reg 						[DATA_WIDTH-1:0] registers [0:DEPTH-1];
		reg 						dmuxRegisters [0:DEPTH-1] ;
		integer 					i;
		reg 						ms_bit;

	counter counter_inst
	(
		.clk		(clk),
		.reset		(reset),
		.increment	(push),
		.count		(wr_ptr)

	);
	counter counter_inst1
	(
		.clk		(clk),
		.reset		(reset),
		.increment	(pop),
		.count		(rd_ptr)

	);


	always @ (posedge clk) 
	begin
		
		for (i = 0; i < DEPTH; i = i + 1) 
			begin
				if (reset)
				registers[i] <= 0;
				else if (push)
				registers[wr_ptr]<= dataIn;
			end
	end


	assign fifoEmpty =  (wr_ptr[COUNT_WIDTH] == rd_ptr[COUNT_WIDTH])& (wr_ptr[COUNT_WIDTH-1:0] == rd_ptr[COUNT_WIDTH-1:0]);
	assign fifoFull = ~(wr_ptr[COUNT_WIDTH] == rd_ptr[COUNT_WIDTH])& (wr_ptr[COUNT_WIDTH-1:0] == rd_ptr[COUNT_WIDTH-1:0]);
	assign dataOutput = registers[rd_ptr];

	endmodule

