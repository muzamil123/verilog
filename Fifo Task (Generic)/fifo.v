module fifo 
	#( 
		parameter 		DATA_WIDTH = 16,
		parameter 		DEPTH = 32,
		parameter 		COUNT_WIDTH = $clog2(DEPTH)
	)
	(
		input 			clk,
		input 			reset,
		input 			[DATA_WIDTH-1:0] dataIn,
		input 			push,
		input 			pop,
		output  		fifoFull,
		output  		fifoEmpty,
		output  		[DATA_WIDTH-1:0] dataOutput
	   
	);

		wire 			[COUNT_WIDTH-1:0] count;
		integer 		i;
		reg 			[DATA_WIDTH-1:0] registers [0:DEPTH-1];

	counter counter_inst
	(
		.clk			(clk),
		.reset			(reset),
		.increment		(push),
		.decrement		(pop),
		.count			(count)

	);

	always @(posedge clk) 
		begin
			if(reset)
				begin
					for(i=0;i<DEPTH;i=i+1)
					begin
						registers[i]<=0;
					end
				end
			else if(push)
				begin 
					registers[0]<=dataIn;
					for(i =1 ; i <DEPTH;i=i+1)
						begin
							registers[i]<=registers[i-1];
						end
				end
		end


		
		
		assign dataOutput = registers[count];
		assign fifoEmpty = (count == 0);
		assign fifoFull = (count == DEPTH - 1);


	
endmodule
