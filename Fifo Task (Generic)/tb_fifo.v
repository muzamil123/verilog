module tb_fifo;    
	reg 			clk;
	reg 			reset;
	reg 			[DATA_WIDTH-1:0] dataIn;
	wire 			[DATA_WIDTH-1:0] dataOutput;
	wire 			fifoFull;
	wire 			fifoEmpty;
	reg 			push;
	reg 			pop;
	parameter 		DATA_WIDTH=16;
	parameter 		DEPTH = 32;
	parameter 		COUNT_WIDTH = $clog2(DEPTH);
	integer 		i=0;
	fifo 
	#(
		.DATA_WIDTH		(DATA_WIDTH),
		.COUNT_WIDTH	(COUNT_WIDTH)
	) 
	fifo_instance 
	(
		.clk			(clk),
		.reset			(reset),
		.dataOutput		(dataOutput),
		.dataIn			(dataIn),
		.push			(push),
		.pop			(pop)
	);

	initial begin
		clk = 0;
		forever #5 clk = ~clk;
	end

	initial begin
		$dumpfile("fifo_wave.vcd");
		$dumpvars(1,fifo_instance);

		reset = 1;
		@(posedge clk);
		#1 reset = 0;
		 pop=0;
		 push = 0;
		repeat (31)
		begin
			@(posedge clk);
			#1 push = 1;
			dataIn = i+1;
		end
		@(posedge clk);
		#1 push = 0;
		@(posedge clk);
		#1 pop = 1;
		@(posedge clk);
		#1 pop = 1;
		 @(posedge clk);
		#1 pop = 1;
		@(posedge clk);
		#1 pop = 1;
		#1 pop = 1;
		@(posedge clk);
		#1 pop = 1;
		 @(posedge clk);
		#1 pop = 1;
		@(posedge clk);
		#1 pop = 1;
		@(posedge clk);
		#1 pop=0;
		repeat (20) @(posedge clk);

		$finish;
	end

endmodule